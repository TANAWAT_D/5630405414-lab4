package dathtanim.tanawat.lab4;
/*
 * AppleDevice
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class AppleDevice extends MobileDevice {

	private static String brand = "Apple";
	private String iOSVersion = "";


	public AppleDevice(String modelName, String os, double price, double rating, double weight) {
		super(modelName, os, price, rating, weight);

	}

	public AppleDevice(String modelName, String os, int price, double rating, int weight, String iOSVersion) {
		super(modelName, os, price, rating, weight);
		this.iOSVersion = iOSVersion;
	}

	public static String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		AppleDevice.brand = brand;
	}

	public String getiOSVersion() {
		return iOSVersion;
	}

	public void setiOSVersion(String iOSVersion) {
		this.iOSVersion = iOSVersion;
	}

	@Override
	public String toString() {
		String text = super.toString();
		if (!iOSVersion.equals("")) {
			text += iOSVersion;
		}
		return text;
	}

}
