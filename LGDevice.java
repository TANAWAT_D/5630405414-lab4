package dathtanim.tanawat.lab4;
/*
 * LGDevice
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class LGDevice extends MobileDevice {

	private static String brand = "LG";
	private String androidVersion = "";

	public LGDevice(String modelName, String os, int price, double rating, int weight) {
		super(modelName, os, price, rating, weight);
	}

	public LGDevice(String modelName, String os, int price, double rating, int weight, String androidVersion) {
		super(modelName, os, price, rating, weight);
		this.androidVersion = androidVersion;
	}

	public static String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		LGDevice.brand = brand;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	@Override
	public String toString() {
		String text = super.toString();
		if (!androidVersion.equals("")) {
			text += androidVersion;
		}
		return text;
	}

}
