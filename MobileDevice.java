package dathtanim.tanawat.lab4;
/*
 * MobileDevices
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class MobileDevice {

	private String modelName;
	private String os;
	private double price;
	private double rating;
	private double weight;

	public MobileDevice(String modelName, String os, double price, double rating, double weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.rating = rating;
		this.weight = weight;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		String text = "MobileDevice [modelName=" + modelName + ", os=" + os + ", price=" + price + ", rating=" + rating
				+ ", weight=" + weight + "]";
		return text;
	}

}
