package dathtanim.tanawat.lab4;
/*
 * FavoriteDevices
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class FavoriteDevices {

	public static void main(String[] args) {
		LGDevice g3 = new LGDevice("LG G3", "Android", 16900, 9.5, 149, "Android 4.4 (KitKat)");
		MobileDevice ipadmini3 = new AppleDevice("Apple iPad Mini 3", "iOS", 13400, 8, 312, "iOS8");
		AppleDevice iphone6 = new AppleDevice("Apple iPhone6", "iOS", 24900, 9, 129, "iOS8");
		System.out.println(g3.getModelName() + " has the price different from " + ipadmini3.getModelName() + " as "
				+ getPriceDiff(g3, ipadmini3) + " Baht.");
		System.out.println(iphone6.getModelName() + " has the price different from " + ipadmini3.getModelName() + " as "
				+ getPriceDiff(iphone6, ipadmini3) + " Baht.");

	}

	private static double getPriceDiff(MobileDevice m1, MobileDevice m2) {
		return Math.abs(m1.getPrice() - m2.getPrice());
	}
}
