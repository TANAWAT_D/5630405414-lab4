package dathtanim.tanawat.lab4;
/*
 * LGDevices2014
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */
public class LGDevices2014 {

	public static void main(String[] args) {
		MobileDevice g3 = new LGDevice("LG G3", "Android", 16900, 9.5, 149);
		LGDevice g2mini = new LGDevice("LG G2 mini", "Android", 8900, 4.5, 121, "Android 4.4 (KitKat)");
		System.out.println("I would like to have ");
		System.out.println(g3);
		System.out.println("But to save money, I should buy ");
		System.out.println(g2mini);
		double diff = g3.getPrice() - g2mini.getPrice();
		System.out.println("G2 mini is cheaper than G3 by " + diff + " Baht.");
		System.out.println("Both these device have the same brand which is " + LGDevice.getBrand());
	}
}
